import hospital.*;

import hospital.exception.RemoveException;
import hospital.person.Employee;
import hospital.person.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;


@DisplayName("Department test - testing removal method")
public class DepartmentTest {

    private Department department;

    @BeforeEach
    public void createCommonObjects() {

        department = new Department("Akutten");
        //Instantiate some employees
        Employee employee1 = new Employee("Odd Even", "Primtallet", "123");
        Employee employee2 = new Employee("Huppasahn", "DelFinito", "456");
        Employee employee3 = new Employee("Rigmor", "Mortis", "789");
        //Instantiate some patients
        Patient patient1 = new Patient("Inga", "Lykke", "987");
        Patient patient2 = new Patient("Ulrik", "Smål", "654");
        Patient patient3 = new Patient("Laila", "La", "321");
        //Add some employees
        department.addEmployee(employee1);
        department.addEmployee(employee2);
        department.addEmployee(employee3);
        //Add some patients
        department.addPatient(patient1);
        department.addPatient(patient2);
        department.addPatient(patient3);
    }

    @DisplayName("Removing a employee existing in the register with valid info")
    @Test
    public void testIfExistingEmployeeIsRemoved() {

        int numberBeforeRemoval = department.getEmployees().size();

        try {

            department.removePerson(department.getEmployees().get(0));

        } catch (RemoveException re) {
            //Using fail() to show unexpected failure
            fail("Error message. The existing employee was not removed");
            re.printStackTrace();
        }
        int numberAfterRemoval = department.getEmployees().size();

        assertEquals(numberBeforeRemoval - 1, numberAfterRemoval);

    }

    @DisplayName("Removing a patient existing in the register with valid info")
    @Test
    public void testIfExistingPatientIsRemoved() {

        int numberBeforeRemoval = department.getPatients().size();

        try {
            department.removePerson(department.getPatients().get(0));

        } catch (RemoveException re) {
            //Using fail() to show unexpected failure
            fail("Error message. The existing patient was not removed");
            re.printStackTrace();
        }
        int numberAfterRemoval = department.getPatients().size();

        assertEquals(numberBeforeRemoval - 1, numberAfterRemoval);

    }

    @DisplayName("Removing a non existing person with valid info")
    @Test
    public void testNonExistingPersonWithValidInput() {

        String firstName = "Kari";
        String lastName = "Nilsen";
        String socialSecurityNumber = "112";
        //No difference if Patient or Employee is used. In this test an instance of Employee class is used
        Employee nonExistingEmployee = new Employee(firstName,
                lastName, socialSecurityNumber);


        assertThrows(RemoveException.class, () -> department.removePerson(nonExistingEmployee));


    }

    @DisplayName("Removing non existing employee with parameters matching a patient")
    @Test
    public void testIfNonExistingEmployeeIsRemovedWithParametersMatchingPatient(){

        String firstName = "Jonas";
        String lastName = "Olsen";
        String socialSecurityNumber = "872";
        department.addPatient(new Patient(firstName,lastName,socialSecurityNumber ));

        int numberOfPatientsBefore = department.getPatients().size();

        assertThrows(RemoveException.class,() -> department.removePerson(
                new Employee(firstName,lastName,socialSecurityNumber)));

        int numberOfPatientsAfter = department.getPatients().size();

        //Checks if the number of employees has not decreased after attempted removal.
        assertEquals(numberOfPatientsBefore,numberOfPatientsAfter);
    }

    @DisplayName("Removing non existing patient with parameters matching an employee")
    @Test
    public void testIfNonExistingPatientIsRemovedWithParametersMatchingEmployee(){

        String firstName = "Jonas";
        String lastName = "Olsen";
        String socialSecurityNumber = "872";
        department.addEmployee(new Employee(firstName,lastName,socialSecurityNumber ));

        int numberOfEmployeesBefore = department.getEmployees().size();

        assertThrows(RemoveException.class,() -> department.removePerson(
                new Patient(firstName,lastName,socialSecurityNumber)));

        int numberOfEmployeesAfter = department.getEmployees().size();

        //Checks if the number of employees has not decreased after attempted removal.
        assertEquals(numberOfEmployeesBefore,numberOfEmployeesAfter);
    }

    @DisplayName("Removing null object")
    @Test
    public void testIfRemovingNullObjectThrowsRemoveException(){

        assertThrows(RemoveException.class, () -> department.removePerson(null));
    }

}
