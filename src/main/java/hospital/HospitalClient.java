package hospital;


import hospital.exception.RemoveException;
import hospital.person.Employee;
import hospital.person.Patient;


/**
 * The type Hospital client.
 */
public class HospitalClient {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        Hospital hospital = new Hospital("Akershus universitetssykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);

        Department department = hospital.getDepartments().get(0);
        Patient patientToBeRemoved = new Patient("Lars", "Tidemann", "183");
        Employee employeeToBeRemoved = department.getEmployees().get(0);

        try {

            //Should succeed
            department.removePerson(employeeToBeRemoved);

            //Should fail
            department.removePerson(patientToBeRemoved);

        } catch (RemoveException re) {
            System.err.println("Removal unsuccessful. More info: " + re.getMessage());
        }
    }
}
