package hospital;

import hospital.person.Employee;
import hospital.person.healthpersonal.doctor.GeneralPractitioner;
import hospital.person.healthpersonal.Nurse;
import hospital.person.Patient;
import hospital.person.healthpersonal.doctor.Surgeon;

/**
 * The type Hospital test data.
 * from the task description provided
 */
public final class HospitalTestData {


    private HospitalTestData() {
        // not called
    }

    /**
     * Fill register with test data.
     *
     * @param hospital the hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "111"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "112"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "113"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "114"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "115"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "116"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "117"));
        emergency.addPatient(new Patient("Inga", "Lykke", "118"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "119"));
        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "110"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "333"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "123"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "1987"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "124"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "125"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "126"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "127"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "128"));
        hospital.getDepartments().add(childrenPolyclinic);

    }
}
