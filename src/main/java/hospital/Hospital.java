package hospital;

import java.util.ArrayList;

/**
 * The type Hospital holding objects of Department class.
 *
 * @author Karishma Sharma
 */
public class Hospital {

    private final String hospitalName;
    private ArrayList<Department> departments;

    /**
     * Instantiates a new Hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        if (hospitalName.isBlank()) {
            throw new IllegalArgumentException("Invalid hospital name. Cannot be blank");
        } else {
            this.hospitalName = hospitalName;
        }
        departments = new ArrayList<>();
    }

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Add department to the Hospital.
     *
     * @param newDepartment the new department
     */
    public void addDepartment(Department newDepartment) {

        if (null != newDepartment) {
            if (!departments.contains(newDepartment)) {
                departments.add(newDepartment);
            }
        }
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Hospital name: ").append(this.hospitalName);
        stringBuilder.append("\nDepartments:\n");
        for (Department department : departments) {
            stringBuilder.append(department.toString()).append("\n");
        }

        return stringBuilder.toString();
    }
}
