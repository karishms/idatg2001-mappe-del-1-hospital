package hospital.exception;

/**
 * The type Remove exception.
 * @author Karishma Sharma
 */
public class RemoveException extends Exception {

    private final static long serialVersionUID = 1L;

    /**
     * Instantiates a new Remove exception.
     *
     * @param exceptionMessage the exception message
     */
    public RemoveException(String exceptionMessage){
        super(exceptionMessage);
    }

}
