package hospital.person;

import java.util.Objects;

/**
 * A abstract class holding all the necessary information about a person.
 * @author Karishma Sharma
 */
public abstract class Person {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Instantiates a new Person.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Person(String firstName, String lastName, String socialSecurityNumber){

      setFirstName(firstName);
      setLastName(lastName);
      setSocialSecurityNumber(socialSecurityNumber);

    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
       if(firstName.isBlank()){
           throw new IllegalArgumentException("First name cannot be blank.");
       } else this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        if(lastName.isBlank()){
            throw new IllegalArgumentException("Last name cannot be blank.");
        } else this.lastName = lastName;
    }

    /**
     * Gets social security number.
     *
     * @return the social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Sets social security number.
     *
     * @param socialSecurityNumber the social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if(socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("Social security number cannot be blank.");
        } else this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Get full name as a string.
     *
     * @return the full name string
     */
    public String getFullName(){
        return this.firstName + " " + this.lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) && Objects.equals(lastName, person.lastName) && Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\tName: ").append(getFullName());
        stringBuilder.append("\n\tSocial security number: ").append(this.socialSecurityNumber);

        return stringBuilder.toString();
    }


}
