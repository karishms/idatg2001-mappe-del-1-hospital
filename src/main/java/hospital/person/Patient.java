package hospital.person;


/**
 * The type Patient.
 * @author Karishma Sharma
 */
public class Patient extends Person implements Diagnosable {

    private String diagnosis = "";

    /**
     * Instantiates a new Patient.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        //The class diagram from the project description says that the constructor is supposed to be
        //protected. But because of the package structure I have decided to have, this is not convenient.
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Gets diagnosis.
     *
     * @return the diagnosis
     */
    protected String getDiagnosis() {
        return this.diagnosis;
    }

    /**
     * Sets diagnosis
     * @param diagnosisToBeSet the diagnosis to be set
     */
    public void setDiagnosis(String diagnosisToBeSet) {
        if (diagnosisToBeSet.isBlank()){
            throw new IllegalArgumentException("The diagnosis cannot be blank.");
        }
        else this.diagnosis = diagnosisToBeSet;
    }

    @Override
    public String toString() {

        String patientToString;

        if (!this.diagnosis.isEmpty()) {
            patientToString = super.toString() + "\n\tDiagnosis: " + this.diagnosis;
        } else {
            patientToString = super.toString() + "\n\tNo diagnosis.";
        }

        return patientToString;
    }

}

