package hospital.person.healthpersonal.doctor;

import hospital.person.Patient;

/**
 * The type Surgeon.
 * @author Karishma Sharma
 */
public class Surgeon extends Doctor {

    /**
     * Instantiates a new Surgeon.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }


    public void setDiagnosis(Patient patientToBeDiagnosed, String diagnosis){
        patientToBeDiagnosed.setDiagnosis(diagnosis);
    }

}
