package hospital.person.healthpersonal.doctor;

import hospital.person.Employee;
import hospital.person.Patient;

/**
 * The type Doctor.
 * @author Karishma Sharma
 */
public abstract class Doctor extends Employee {

    /**
     * Instantiates a new Doctor.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }


    /**
     * Sets diagnosis.
     *
     * @param patientToBeDiagnosed the patient to be diagnosed
     * @param diagnosis            the diagnosis
     */
    public abstract void setDiagnosis(Patient patientToBeDiagnosed, String diagnosis);
}
