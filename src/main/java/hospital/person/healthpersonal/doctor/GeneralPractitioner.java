package hospital.person.healthpersonal.doctor;

import hospital.person.Patient;

/**
 * The type General practitioner.
 * @author Karishma Sharma
 */
public class GeneralPractitioner extends Doctor {

    /**
     * Instantiates a new General practitioner.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }


    public void setDiagnosis(Patient patientToBeDiagnosed, String diagnosis){
        patientToBeDiagnosed.setDiagnosis(diagnosis);
    }
}