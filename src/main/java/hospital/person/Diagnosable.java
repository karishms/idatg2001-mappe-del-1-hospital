package hospital.person;

/**
 * The interface Diagnosable.
 * @author Karishma Sharma
 */
public interface Diagnosable {

    /**
     * Sets diagnosis.
     *
     * @param diagnosisToBeSet the diagnosis to be set
     */
    public void setDiagnosis(String diagnosisToBeSet);

}
