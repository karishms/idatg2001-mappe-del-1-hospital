package hospital;

import hospital.exception.RemoveException;
import hospital.person.Employee;
import hospital.person.Patient;
import hospital.person.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


/**
 * The Department class holding all employees and patients in the department
 * @author Karishma Sharma
 */
public class Department {

    private String departmentName;
    //Chose HashMap using the social security number (String) as key because its unique for every person.
    private HashMap<String, Employee> employees;
    private HashMap<String, Patient> patients;

    /**
     * Instantiates a new Department.
     *
     * @param departmentName the department name
     */
    public Department(String departmentName) {
        setDepartmentName(departmentName);
        employees = new HashMap<>();
        patients = new HashMap<>();
    }

    /**
     * Gets department name.
     *
     * @return the department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        if (departmentName.isBlank()) {
            throw new IllegalArgumentException("Invalid department name. Cannot be blank.");
        } else {
            this.departmentName = departmentName;
        }
    }

    /**
     * Gets employees as an ArrayList.
     *
     * @return the employees
     */
    public List<Employee> getEmployees() {
        return new ArrayList<>(employees.values());
    }

    /**
     * Gets patients as an ArrayList.
     *
     * @return the patients
     */
    public List<Patient> getPatients() {
        return new ArrayList<>(patients.values());
    }

    /**
     * Adds employee to the employee map.
     *
     * @param newEmployee the new employee
     */
    public void addEmployee(Employee newEmployee) {
        //Will not add a null value. Therefore not necessary with a guard condition.
        employees.putIfAbsent(newEmployee.getSocialSecurityNumber(), newEmployee);
    }


    /**
     * Add patient to the patient map.
     *
     * @param newPatient the new patient
     */
    public void addPatient(Patient newPatient) {
        //Will not add a null value. Therefore not necessary with a guard condition
        patients.putIfAbsent(newPatient.getSocialSecurityNumber(), newPatient);
    }

    /**
     * Remove person from map, depending of which instance is passed. Throws Remove Exception if the person does not
     * exist in any register, if the department is empty or if the given register i empty.
     *
     * @param personToBeRemoved the person to be removed
     * @throws RemoveException the remove exception
     */
    public void removePerson(Person personToBeRemoved) throws RemoveException {

        if (null == personToBeRemoved) {
            throw new RemoveException("Person cannot be null");
        }

        //Checks if the input object is an instance of Employee
        if (personToBeRemoved instanceof Employee) {
            //Checks if the employee register is empty
            if (employees.isEmpty()) {
                throw new RemoveException("There are no employees in this department.");

            } else {
                if (employees.containsKey(personToBeRemoved.getSocialSecurityNumber())) {
                    employees.remove(personToBeRemoved.getSocialSecurityNumber(), personToBeRemoved);
                } else {
                    throw new RemoveException("This employee does not exist");
                }

            }

            //Checks if the input object is an instance of Patient
        } else if (personToBeRemoved instanceof Patient) {
            //Checks if the patient register is empty
            if (patients.isEmpty()) {
                throw new RemoveException("There are no patients in this department.");
            } else {
                if (patients.containsKey(personToBeRemoved.getSocialSecurityNumber())) {
                    patients.remove(personToBeRemoved.getSocialSecurityNumber(), personToBeRemoved);
                } else {
                    throw new RemoveException("This patient does not exist");
                }
            }

        } else
            throw new RemoveException("This person is not in any register.");

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Department name: ").append(this.departmentName);
        stringBuilder.append("\nEmployees:\n");
        for (Employee employee : employees.values()) {
            stringBuilder.append(employee.toString()).append("\n");
        }

        stringBuilder.append("\nPatients:\n");
        for (Patient patient : patients.values()) {
            stringBuilder.append(patient.toString()).append("\n");
        }
        return stringBuilder.toString();
    }


}
